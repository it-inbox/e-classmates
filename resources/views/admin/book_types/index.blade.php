@extends('admin.template.layout')
@section('title','Book Types')
@section('page-content')
@breadcrumb(Dashboard:admin-dashboard,Book Types:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 text-right mb-3">
                        <a href="{{ route('book_types_create') }}" class="btn btn-primary pull-right">Add Book Types</a>
                    </div>
                </div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($Books as $index => $Book)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $Book -> name }}</td>
                            <td>
                                @if($Book->status == '1')
                                    <span class="badge badge-primary">Active</span>
                                @else
                                    <span class="badge badge-danger">Inactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('book_types_edit', $Book->id) }}" class="btn btn-warning" title="Edit Publisher"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                  
                </table>
            </div>
        </div>
    </div>
@endsection
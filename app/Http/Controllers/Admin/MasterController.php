<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ChannelPreference;
use App\Models\Currency;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Activitylog\Models\Activity;

class MasterController extends Controller
{

    public function languageIndex(){
        $languages = Language::orderBy('id','desc')->get();

        return view('admin.books.language.index',[
            'languages' => $languages
        ]);
    }

    public function languageCreate(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'name' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            Language::create([
                'name' => $request->name,
                'slug' =>  \Str::slug($request->name,'-'),
                'status' => Language::ACTIVE
            ]);
            return redirect()->route('book_languages_list_view')->with(['success' => 'Language Added Successfully!']);
        }
        return view('admin.books.language.create');
    }

    public function languageUpdate(Request $request,$id){

        $language = Language::where('id',$id)->first();

        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'name' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $language->name =  $request->name;
            $language->slug =  \Str::slug($request->name,'-');
            $language->status = $request->status;
            $language->save();

            return redirect()->route('book_languages_list_view')->with(['success' => 'Language updated Successfully!']);
        }

        return view('admin.books.language.update',[
            'language' => $language
        ]);
    }
}

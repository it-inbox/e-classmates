<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('site_logo');
            $table->string('site_fav_logo');
            $table->string('company_name');
            $table->string('company_email');
            $table->string('company_mobile');
            $table->string('company_address');
            $table->string('pan_number')->nullable();
            $table->string('gst_no')->nullable();
            $table->string('pin_code');
            $table->string('city');
            $table->string('state');
            $table->string('country');
            $table->string('facebook_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('instagram_link')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}

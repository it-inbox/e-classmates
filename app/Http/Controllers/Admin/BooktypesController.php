<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\BookType;

class BooktypesController extends Controller
{
    public function index()
    {
        $Books = BookType::all();
        return view('admin.book_types.index', compact('Books'));
    }

    public function create()
    {
        return view ('admin.book_types.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required'
        ]);

        $bookType = BookType::create($validatedData);

        return redirect()->route('book_types_list')->with('success', 'Book type added successfully');
    }

    public function edit($id)
    {
        $books = BookType::findOrFail($id);
        return view ('admin.book_types.update', compact('books'));
    }



    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required'
            
        ]);
        $books = BookType::where('id',$request->id)->first();
        $books->name = $request->name;
        $books->status = ($request->status == '1'  ? BookType::ACTIVE : BookType::INACTIVE);
        $books->save();

       

        return redirect()->route('book_types_list')->with('success', 'Book Type updated successfully');
    }





   



}

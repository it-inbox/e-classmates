<?php

namespace App\Http\Controllers\Admin;

use App\Models\Publisher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PublisherController extends Controller
{
    public function index(){
        $publishers = Publisher::orderBy('id','desc')->paginate(10);

        return view('admin.publisher.index',[
            'publishers' => $publishers
        ]);
    }

    public function create(){
        return view('admin.publisher.create');
    }

    public function store(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'name' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            Publisher::create([
                'name' => $request->name,
                'slug' =>\Str::slug($request->name,'-'),
                'status' => Publisher::ACTIVE
            ]);

            return redirect()->route('publisher-list-view')->with('success','Publisher Created Successfully');
        }
    }

    public function edit($id){
        return view('admin.publisher.update',[
            'publisher' => Publisher::where('id',$id)->first()
        ]);
    }

    public function update(Request $request)
    {
        $publisher = Publisher::where('id',$request->id)->first();

        if ($request->isMethod('post')) {

            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $publisher->name = $request->name;
            $publisher->slug = \Str::slug($request->name, '-');
            $publisher->status = $request->status;
            $publisher->save();

            return redirect()->route('publisher-list-view')->with('success', 'Publisher Updated Successfully');
        }
    }
}

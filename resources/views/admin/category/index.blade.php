@extends('admin.template.layout')

@section('title', 'Category')

@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Category:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 pull-right mb-3">
                        <a href="{{ route('admin-category-create') }}" class="btn btn-primary pull-right"> Create New Category</a>
                    </div>
                </div>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th width="30%">Name</th>
                        <th>Parent</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($categories as $index => $category)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td><a href="{{ route('admin-category-view' ,['parent_id' => $category->id]) }}">{{ $category->name }}</a></td>
                            <td>{{ $category->parent_id ? $category->parent->name : 'N.A' }}</td>
                            <td>
                                @if ($category->status == 1)
                                    <span class="badge badge-primary"> Active</span>
                                @else
                                    <span class="badge badge-danger"> In Active</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('admin-category-update', ['id' => $category->id ]) }}" class="btn btn-info btn-md"> Edit</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-8">
            </div>
            <div class="col-md-4 text-right">
                @if ($categories->lastPage() > 1)
                    <ul class="pagination">
                        <li class="{{ ($categories->currentPage() == 1) ? ' disabled' : '' }}">
                            <a href="{{ $categories->url(1) }}" class="btn btn-default">Previous</a>
                        </li>
                        @for ($i = 1; $i <= $categories->lastPage(); $i++)
                            <li class="{{ ($categories->currentPage() == $i) ? ' active' : '' }}">
                                <a href="{{ $categories->url($i) }}" class="circle">{{ $i }}</a>
                            </li>
                        @endfor
                        <li class="{{ ($categories->currentPage() == $categories->lastPage()) ? ' disabled' : '' }}">
                            <a href="{{ $categories->url($categories->currentPage()+1) }}" class="btn btn-default">Next</a>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
    </div>

@stop
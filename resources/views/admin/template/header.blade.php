<div class="header">
    <a href="javascript:void(0);" class="btn-link toggle-sidebar d-lg-none  pg-icon btn-icon-link" data-toggle="sidebar">
        menu</a>
    <div class="">
        <div class="brand inline">
            {{--@if(isset($setting->site_logo) && !empty($setting->site_logo))--}}
                {{--<img src="{{ asset($setting->site_logo) }}" alt="logo" class="brand" data-src="{{ asset($setting->site_logo) }}" data-src-retina="{{ asset('assets/img/logo_2x.png')  }}" width="100">--}}
            {{--@else--}}
                {{--<img src="{{ asset('admin-assets/company/logo.png') }}" alt="logo" class="brand" data-src="{{ asset('admin-assets/company/logo.png') }}" data-src-retina="{{ asset('assets/img/logo_2x.png')  }}" width="100">--}}
            {{--@endif--}}
        </div>
        <a href="{{  route('admin-dashboard')  }}" class="btn btn-link text-primary m-l-20 d-none d-lg-inline-flex d-xl-inline-flex" target="_blank">View Website</a>
        <a href="javascript:void(0);" class="search-link d-none d-lg-inline-block d-xl-inline-block" data-toggle="search"><i
                    class="pg-icon">search</i>Type anywhere to <span class="bold">search</span></a>
    </div>
    <div class="d-flex align-items-center">
        <span></span>
        <div class="pull-left p-r-10 fs-14 d-lg-inline-block d-none m-l-20">
            <span class="semi-bold hidden-xs hidden-sm text-danger">Last Login: {{ \Session::get('admin')->last_logged_in_at ? \Carbon\Carbon::parse(\Session::get('admin')->last_logged_in_at)->format('M d, Y H:i A') : 'N.A' }}</span>
            <span class="semi-bold"> | {{ \Session::get('admin')->username }}</span>
        </div>
        <div class="dropdown pull-right d-lg-block d-none">
            <button class="profile-dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="profile dropdown">
              <span class="thumbnail-wrapper d32 circular inline">
      					<img src="{{ asset('admin-assets/img/profiles/avatar.jpg')  }}" alt="" data-src="{{ asset('admin-assets/img/profiles/avatar.jpg')  }}"
                             data-src-retina="{{ asset('admin-assets/img/profiles/avatar_small2x.jpg')  }}" width="32" height="32">
      				</span>
            </button>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                <a href="{{ route('admin-manager-profile')  }}" class="dropdown-item"><i class="fa fa-user"></i> Profile</a>
                <a href="{{ route('admin-logout') }}" class="clearfix bg-master-lighter dropdown-item">
                    <span class="pull-left">Logout</span>
                </a>
            </div>
        </div>
    </div>
</div>
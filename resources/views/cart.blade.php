<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cart</title>
    <link rel="stylesheet" href="{{ asset('admin-assets/plugins/bootstrap/css/bootstrap.min.css')  }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div class="container">
    <table class="table table-hover table-condensed table-bordered mt-5 text-center">
        <thead>
        <tr>
            <th>Product</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Subtotal</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @php $total = 0 @endphp
        @foreach(session('cart') as $details)
            @php $total += $details['price'] * $details['quantity'] @endphp
            <tr>
                <td>
                    <div class="row">
                        <div class="col-sm-3 hidden-xs"><img src="{{ asset($details['image']) }}" width="100" height="100" class="img-responsive"/></div>
                        <div class="col-sm-3 offset-sm-1">
                            <h4 class="no-margin">{{ $details['name'] }}</h4>
                        </div>
                    </div>
                </td>
                <td>₹ {{ $details['price']  }}</td>
                <td>{{ $details['quantity']  }}</td>
                <td>₹ {{ $details['price'] * $details['quantity'] }}</td>
                <td><button class="btn btn-danger btn-sm remove-from-cart"><i class="fa fa-trash-o"></i></button></td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="5" class="float-right mx-auto"><h3><strong>Total ₹{{ $total }}</strong></h3></td>
        </tr>
        <tr>
            <td colspan="5" class="text-right">
                <a href="{{ url('/') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a>
                <a href="{{ route('checkout')  }}"  class="btn btn-success">Checkout</a>
            </td>
        </tr>
        </tfoot>
    </table>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $('.remove-from-cart').click(function(e){
        e.preventDefault();
        val ele = $(this);

        if(confirm("Are you sure want to remove?")){
            alert('oke');
        }
    });
</script>
</body>
</html>
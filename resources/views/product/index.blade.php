<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Index</title>
    <link rel="stylesheet" href="{{ asset('admin-assets/plugins/bootstrap/css/bootstrap.min.css')  }}">
</head>
<body>
<div class="container">
    <h5 class="text-center mt-5">Books</h5>
    <a href="{{ route('view-cart')  }}" class="btn btn-info float-end mb-5">Add To Cart {{ count((array) session('cart')) }}</a>
    <div class="border-1 mt-3 mb-0" style="border-top: 1px solid">
        <div class="row align-items-center">
            @if(count($books) > 0)
                @foreach($books as $book)
                    <div class="col-md-4">
                        <div class="card mt-5">
                            <div class="card-body text-center">
                                <img src="{{ asset($book->book_image)  }}" alt="" width="100px">
                                <h5>{{ $book->book_name  }}</h5>
                                <a href="{{ route('add.to.cart',$book->id)  }}" class="btn btn-success">Add To Cart</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="col-md-12">
                    <div class="text-center">
                        <h5>No Books Available.!!!</h5>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
</body>
</html>
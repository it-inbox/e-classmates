@extends('admin.template.layout')
@section('title','User Create')
@section('page-content')
    <div class="content">
        <div class="jumbotron" data-pages="parallax">
            <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
                <div class="inner">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin-dashboard')  }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('user-list-view')  }}">User</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="container-fluid container-fixed-lg">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body p-4 m-0">
                            <form action="{{ route('user-list-store')  }}" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group form-group-default">
                                                    <label>First Name</label>
                                                    <input type="text" class="form-control" name="first_name">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group form-group-default">
                                                    <label>Middle Name</label>
                                                    <input type="text" class="form-control" name="middle_name">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group form-group-default">
                                                    <label>Last Name</label>
                                                    <input type="text" class="form-control" name="last_name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-4">
                                            <div class="col-lg-4">
                                                <div class="form-group form-group-default">
                                                    <label>UserName</label>
                                                    <input type="text" class="form-control" name="username">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group form-group-default">
                                                    <label>Email</label>
                                                    <input type="email" class="form-control" name="email">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group form-group-default position-relative">
                                                    <label>Password</label>
                                                    <input type="password" class="form-control" id="dz-password" name="password">
                                                    <span class="show-pass eye">
                                                    <i class="fa fa-eye-slash"></i>
                                                    <i class="fa fa-eye"></i>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-4">
                                            <div class="col-lg-3">
                                                <div class="form-group form-group-default">
                                                    <label>Country</label>
                                                    <select name="country" class="form-control">
                                                        <option value="">Select Country</option>
                                                        @foreach($countries as $index => $country)
                                                            <option value="{{ $index  }}">{{ $country  }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group form-group-default">
                                                    <label>Mobile</label>
                                                    <input type="text" class="form-control" name="mobile">
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group form-group-default">
                                                    <label>Gender</label>
                                                    <select name="gender" class="form-control">
                                                        <option value="">Select Gender</option>
                                                        <option value="1">male</option>
                                                        <option value="2">female</option>
                                                        <option value="3">other</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group form-group-default">
                                                    <label>Date Of Birth</label>
                                                    <input type="date" name="date_of_birth" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-4">
                                            <div class="form-group form-group-default">
                                                <label>Address</label>
                                                <textarea name="address" cols="5" rows="5" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="profile-photo">
                                            <label>Profile Picture</label>
                                            <div class="dz-default dlab-message upload-img mb-3">
                                                <div class="fallback">
                                                    <img class="img-circle profile_img" src="" alt="" />
                                                    <input name="profile_photo" type="file" id="profile_photo">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-lg-12 text-center">
                                            <button type="submit" class="btn btn-primary btn-sm">Create</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page-js')
    <script>
        $(document).ready(function(){
            $('#profile_photo').change(function (e) {
                e.preventDefault();
                const file = this.files[0];
                if (file){
                    let reader = new FileReader();
                    reader.onload = function(event){
                        $('.profile_img').attr('src', event.target.result);
                    };
                    reader.readAsDataURL(file);
                }
            })
        });
    </script>
@endsection
@extends('admin.template.layout')
@section('title','Create')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Boards:admin-boards-view,Create:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{ route('admin-boards-store') }}">
                            @csrf
                            <div class="form-group form-group-default">
                                <label for="author">Name<span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}">
                                @error('name')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary btn-md">Submit</button>
                                <a href="{{  route('admin-boards-view') }}" class="btn text-dark cancel-button">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
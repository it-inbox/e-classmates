@extends('admin.template.layout')
@section('title','Edit User')
@section('page-content')
    @breadcrumb(Home:admin-dashboard,User:user-list-view,Update:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body p-4 m-0">
                        <form action="{{ route('user-update')  }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" value="{{ $user->id  }}" name="user_id">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="form-group form-group-default">
                                        <label>Full Name</label>
                                        <input type="text" class="form-control" name="full_name" value="{{old('full_name', $user->full_name)}}">
                                        @error('author')
                                        <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group form-group-default">
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="email" value="{{ old('email',$user->email)  }}">
                                        @error('author')
                                        <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group form-group-default">
                                        <label>Contact Number</label>
                                        <input type="number" class="form-control" name="phone" value="{{ old('mobile',$user->mobile)  }}">
                                        @error('phone')
                                        <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group form-group-default d-none">
                                        <label>Password</label>
                                        <input type="password" class="form-control passwordInput" id="dz-password" name="password">
                                        <div class="input-group-append position-absolute pull-right password-icon">
                                            <button class="btn btn-primary float-right" type="button" onclick="showPassword(this)">
                                                <i class="fa fa-eye-slash"></i>
                                            </button>
                                        </div>
                                        @error('password')
                                        <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group form-group-default">
                                        <label>Address</label>
                                        <textarea name="address" rows="10" class="form-control">{{ old('address',$user->address)  }}</textarea>

                                    </div>
                                    <div class="form-group">
                                        <label for="status">Status</label>
                                        <div class="form-check form-check-inline">
                                            <input type="radio" name="status" id="radioInline1" value="1" {{ $user->status == \App\Models\User::ACTIVE ? 'checked' : ''  }}>
                                            <label for="radioInline1">
                                                Active
                                            </label>
                                        </div> <div class="form-check form-check-inline complete">
                                            <input type="radio" name="status" id="radioDisabled2" value="2" {{ $user->status == \App\Models\User::INACTIVE ? 'checked' : ''  }}>
                                            <label for="radioDisabled2">
                                                In-active
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="profile-photo">
                                        <label>Profile Picture</label>
                                        <div class="dz-default dlab-message upload-img mb-3">
                                            <div class="fallback">
                                                <img class="img-circle profile_img" src="" alt="" />
                                                <input name="profile_photo" type="file" id="profile_photo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="current_photo text-center">
                                        <label>Current Profile</label>
                                        <img src="{{ asset($user->profile_photo)  }}" class="img-circle" alt="">
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-lg-12 text-center">
                                        <button type="submit" class="btn btn-primary btn-md">Update</button>
                                        <a href="{{  route('user-list-view') }}" class="btn text-dark cancel-button">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page-js')
    <script>
        $(document).ready(function(){
            $('#profile_photo').change(function (e) {
                e.preventDefault();
                const file = this.files[0];
                if (file){
                    let reader = new FileReader();
                    reader.onload = function(event){
                        $('.profile_img').attr('src', event.target.result);
                    };
                    reader.readAsDataURL(file);
                }
            })
        });
        function showPassword(element){
            var passwordElement = $(element).parent().parent().find('input.passwordInput');

            var inputType = passwordElement.attr('type');

            if (inputType === 'password') {
                $(element).html('<i class="fa fa-eye"></i>');
                passwordElement.attr('type', 'text');
            }
            else {
                $(element).html('<i class="fa fa-eye-slash"></i>');
                passwordElement.attr('type', 'password');
            }
        }
    </script>
@endsection
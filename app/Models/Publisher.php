<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    use HasFactory;

    const  ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'name','slug','status'
    ];

    public function book(){
        return $this->hasOne(Book::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'site_logo','site_fav_logo','company_name','company_email','company_mobile','company_address','pan_number','gst_no','pin_code','city','state','country','facebook_link','twitter_link','instagram_link'
    ];
}

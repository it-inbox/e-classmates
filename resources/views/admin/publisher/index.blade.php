@extends('admin.template.layout')
@section('title','Publisher')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Publisher:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 text-right mb-3">
                        <a href="{{ route('publisher-list-create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add Publisher</a>
                    </div>
                </div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($publishers as $index => $publisher)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $publisher->name }}</td>
                            <td>
                                @if($publisher->status == '1')
                                    <span class="badge badge-primary">Active</span>
                                @else
                                    <span class="badge badge-danger">In-active</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('publisher-list-edit',$publisher->id) }}" class="btn btn-warning" title="Edit Publisher"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-12 pr-3 mr-3">
                @if ($publishers->lastPage() > 1)
                    <ul class="pagination pull-right">
                        <li class="{{ ($publishers->currentPage() == 1) ? ' disabled' : '' }}">
                            <a href="{{ $publishers->url(1) }}" class="btn btn-default">Previous</a>
                        </li>
                        @for ($i = 1; $i <= $publishers->lastPage(); $i++)
                            <li class="{{ ($publishers->currentPage() == $i) ? 'active' : '' }}">
                                <a href="{{ $publishers->url($i) }}" class="circle">{{ $i }}</a>
                            </li>
                        @endfor
                        <li class="{{ ($publishers->currentPage() == $publishers->lastPage()) ? ' disabled' : '' }}">
                            <a href="{{ $publishers->url($publishers->currentPage()+1) }}" class="btn btn-default">Next</a>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
    </div>
@endsection
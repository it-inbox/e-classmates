@extends('admin.template.layout')
@section('title','Create Category')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Category:admin-category-view,Create:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('admin-category-create') }}" method="post" role="form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Parent Category</label>
                                <select name="parent_category_id" class="full-width" data-init-plugin="select2">
                                    <option value="">Select</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                                @error('parent_category_id')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group form-group-default">
                                <label>Category Name</label>
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                @error('name')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-danger btn-md" type="submit"> Create Category </button>
                                    <a href="{{  route('admin-category-view') }}" class="btn text-dark cancel-button">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Board;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BoardController extends Controller
{
    public function index(){

        $boards = Board::orderBy('id','desc')->get();

        return view('admin.boards.index',[
            'boards' => $boards
        ]);
    }

    public function create(){
        return view('admin.boards.create');
    }

    public function store(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'name' => 'required'
            ]);

            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            Board::create([
                'name' => $request->name,
                'slug' => \Str::slug($request->name,'-')
            ]);

            return redirect()->route('admin-boards-view')->with('success','Board Added Successfully.!!!');
        }
    }

    public function edit($id){
        return view('admin.boards.update',[
            'board' => Board::where('id',$id)->first()
        ]);
    }

    public function update(Request $request){

        $board = Board::where('id',$request->id)->first();

        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'name' => 'required'
            ]);

            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $board->name = $request->name;
            $board->slug = \Str::slug($request->name,'-');
            $board->status = $request->status;
            $board->save();

            return redirect()->route('admin-boards-view')->with('success','Board Detail Updated Successfully.!!!');
        }
    }
}

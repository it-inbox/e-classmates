<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    public function index(){

        $books = Book::where('status',Book::ACTIVE)->get();

        return view('product.index',[
            'books' => $books
        ]);
    }

    public function cart(){
        return view('cart');
    }

    public function addToCart($id){
        $book = Book::where('id',$id)->first();

        $cart = Session::get('cart',[]);

        if(isset($cart[$id])){
            $cart[$id]['quantity']++;
        }else{
            $cart[$id] = [
                'name' => $book->book_name,
                'quantity' => 1,
                'price' => $book->price,
                'image' => $book->book_image
            ];
        }

        Session::put('cart',$cart);
        return redirect()->back()->with('success','Product Added to Cart Successfully');
    }

    public function checkout(){
        if(empty(Session::get('front_user'))){
            return redirect()->route('login');
        }
        return view('checkout');
    }

    public function placeOrder(Request $request){

    }
}

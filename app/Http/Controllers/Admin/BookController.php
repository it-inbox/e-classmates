<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Author;
use App\Models\Board;
use App\Models\Book;
use App\Models\Language;
use App\Models\Publisher;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    public function index(){

        $books = Book::with(['publishers','authors','boards','language','category','book_type'])->orderBy('id','desc')->get();

        return view('admin.books.index',[
            'books' => $books
        ]);
    }

    public function create(){
        $boards  = Board::orderBy('id','desc')->get();
        $languages = Language::orderBy('id','desc')->get();
        $publishers = Publisher::orderBy('id','desc')->get();
        $authors = Author::orderBy('id','desc')->get();
        return view('admin.books.create',[
            'boards' => $boards,
            'languages' => $languages,
            'publishers' => $publishers,
            'authors' => $authors
        ]);
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'book_name' => 'required'
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $image = $request->file('book_image');
        $imageName = '';
        $destinationPath = '';
        if (isset($image)){
            $destinationPath = '/uploads/books/';
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $image->move(public_path().$destinationPath, $imageName);
        }
        Book::create([
            'book_image' => $destinationPath.$imageName,
            'book_name' => $request->book_name,
            'publisher' => $request->publisher,
            'author' => $request->author,
            'board' => $request->board,
            'language' => $request->language,
            'category' => $request->category,
            'publication_year' => Carbon::parse($request->publication_year)->format('Y'),
        ]);

        return redirect()->route('admin-books-list-view')->with('success','Book Added Successfully.!');
    }

    public function edit($id){

        $book = Book::where('id',$id)->first();
        $boards  = Board::orderBy('id','desc')->get();
        $languages = Language::orderBy('id','desc')->get();
        $publishers = Publisher::orderBy('id','desc')->get();
        $authors = Author::orderBy('id','desc')->get();

        return view('admin.books.update',[
            'book' => $book,
            'boards' => $boards,
            'languages' => $languages,
            'publishers' => $publishers,
            'authors' => $authors
        ]);
    }

    public function update(Request $request){
        $book = Book::where('id',$request->id)->first();

        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'book_name' => 'required'
            ]);

            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $book->book_name = $request->book_name;
            $book->publisher = $request->publisher;
            $book->author = $request->author;
            $book->board = $request->board;
            $book->language = $request->language;
            $book->category = $request->category;
            $book->publication_year = Carbon::parse($request->publication_year)->format('Y');
            $book->save();

            return redirect()->route('admin-books-list-view')->with('success','Book Details Updated Successfully.!');
        }
    }
}

@extends('admin.template.layout')
@section('title','User')
@section('page-content')
   @breadcrumb(Dashboard:admin-dashboard,User:active)
   <div class="container-fluid container-fixed-lg">
      <div class="card d-none">
         <div class="card-body">
            <form action="" method="get">
               <div class="row">
                  <div class="col-md-9">
                     <div class="row">
                        <div class="col-md-4">
                           <div class="form-group input-group transparent">
                              <div class="input-group-prepend">
                                 <span class="input-group-text transparent"><i class="pg-icon">search</i></span>
                              </div>
                              <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                           </div>
                        </div>
                        <div class="col-md-1 mt-1">
                           <button class="btn btn-danger"> Search</button>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
      <div class="card">
         <div class="card-body">
            <div class="table-responsive m-t-10">
               <table class="table table-hover table-bordered">
                  <thead>
                  <tr class="text-center">
                     <th>Actions</th>
                      <th>Profile Photo</th>
                     <th>User Name</th>
                     <th>Email</th>
                     <th>Contact Number</th>
                     <th>Gender</th>
                     <th>Status</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($users as $index => $user)
                     <tr class="text-center">
                        <td>
                           <a href="{{ route('edit_user',$user->id)  }}" class="btn btn-warning btn" title="Edit User"><i class="fa fa-pencil"></i></a>
                        </td>
                         <td><img src="{{ asset($user->profile_photo)  }}" alt="Profile Photo" width="48" class="img-rounded"></td>
                        <td>{{ $user->display_name() }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->mobile }}</td>
                        <td>
                           @if($user->gender != '')
                              @if($user->gender == 1 || $user->gender == 'male' || $user->gender == 'Male')
                                 <span class="badge badge-primary light border-0">Male</span>
                              @else
                                 <span class="badge badge-danger light border-0">Female</span>
                              @endif
                           @else
                              <span>N.A</span>
                           @endif
                        </td>
                        <td>
                           @if($user->status == 1)
                              <span class="badge badge-primary light border-0">Active</span>
                           @else
                              <span class="badge badge-danger light border-0">InActive</span>
                           @endif
                        </td>
                     </tr>
                  @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <div class="row mb-5">
         <div class="col-md-8">
            {{--<span>Showing {{ $users->perPage()  }} Of {{ $users->total()  }} Results</span>--}}
         </div>
         <div class="col-md-4 text-right">
            @if ($users->lastPage() > 1)
               <ul class="pagination">
                  <li class="{{ ($users->currentPage() == 1) ? ' disabled' : '' }}">
                     <a href="{{ $users->url(1) }}" class="btn btn-default">Previous</a>
                  </li>
                  @for ($i = 1; $i <= $users->lastPage(); $i++)
                     <li class="{{ ($users->currentPage() == $i) ? ' active' : '' }}">
                        <a href="{{ $users->url($i) }}" class="circle">{{ $i }}</a>
                     </li>
                  @endfor
                  <li class="{{ ($users->currentPage() == $users->lastPage()) ? ' disabled' : '' }}">
                     <a href="{{ $users->url($users->currentPage()+1) }}" class="btn btn-default">Next</a>
                  </li>
               </ul>
            @endif
         </div>
      </div>
   </div>
@stop
@section('page-css')
   <style>
      .pagination{
         float: right;
      }
   </style>
@endsection
@section('page-js')
   <script>
       $('.date-range').daterangepicker({
           locale: {
               format: 'DD MMM YYYY'
           },
           autoUpdateInput: false,
           buttonClasses: ['btn', 'btn-sm'],
           applyClass: 'btn-success',
           cancelClass: 'btn-danger',
           ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
           }
       }).on('apply.daterangepicker', function(ev, picker) {
           $(this).val(picker.startDate.format('DD MMM YYYY') + ' - ' + picker.endDate.format('DD MMM YYYY'));
       }).on('cancel.daterangepicker', function(ev, picker) {
           $(this).val('');
       });

   </script>
@endsection
@extends('admin.template.layout')
@section('title','Update')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Banners:admin-banner-view,Update:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('admin-banner-update')  }}" method="post" role="form" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{  $banner->id }}">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group form-group-default">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" value="{{ old('name',$banner->name) }}">
                                @error('name')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <div class="form-check form-check-inline">
                                    <input type="radio" name="status" id="radioInline1" value="1" {{ $banner->status == 1 ? 'checked' : ''  }}>
                                    <label for="radioInline1">
                                        Active
                                    </label>
                                </div> <div class="form-check form-check-inline complete">
                                    <input type="radio" name="status" id="radioDisabled2" value="2" {{ $banner->status == 2 ? 'checked' : ''  }}>
                                    <label for="radioDisabled2">
                                        In-active
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Select Banner image</label>
                                <input type="file" name="image" id="image">
                                <p><br>1. Upload File Size Not More than 5 MB.</p>
                                <p>2. File Upload Before Getting Smaller Size From <a href="http://tinypng.com" target="_blank">Tinypng.com</a><br/></p>
                                <p class="text-danger">3. For Responsive slider upload image size in 1400*600 pixels</p>
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn btn-primary btn-cons m-b-10"><i class="fa fa-cloud-upload"></i> <span class="bold">Upload</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card card-default">
                                <div class="card-header  separator">
                                    <div class="card-title">Current Banner Image</div>
                                </div>
                                <div class="card-body">
                                    <img src="{{ asset($banner->image) }}" width="100%" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
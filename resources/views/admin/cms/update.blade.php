@extends('admin.template.layout')
@section('title', 'Edit CMS Page')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,CMS:admin-cms-update)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">

                <form action="{{ route('admin-cms-update') }}" method="post">
                    @csrf
                    <input type="hidden" name="id" value="{{ $pagesNames->id }}">
                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input type="text" class="form-control" id="title" name="title" value="{{ $pagesNames->title ? $pagesNames->title : ''   }}" required>
                    </div>


                    <div class="form-group">
                        <label for="description">Description:</label>
                        <textarea class="form-control" id="editor" name="description" required>{{  $pagesNames->description ? $pagesNames->description : ''  }}</textarea>
                    </div>

                    <script src="https://cdn.ckeditor.com/ckeditor5/36.0.0/classic/ckeditor.js">
                    </script>
                    <script>
                        ClassicEditor
                            .create( document.querySelector( '#editor' ) )
                            .catch( error => {
                            console.error( error );
                        } );
                    </script>

                    <div class="form-group">
                        <label for="status">Status:</label>
                        <select class="form-control" id="status" name="status">
                            <option value="1">Active</option>
                            <option value="2">Inactive</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary btn-md">Update</button>
                    <a href="{{  route('admin-cms') }}" class="btn text-dark cancel-button">Cancel</a>
                </form>
            </div>
        </div>
    </div>
@stop

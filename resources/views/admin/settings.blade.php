@extends('admin.template.layout')
@section('title','Settings')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Settings:active)
    <div class="container-fluid container-fixed-lg">
        {{--@if (session()->has('success'))--}}
            {{--<div class="alert-message">--}}
                {{--<div class="alert alert-dismissable alert-success">--}}
                    {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close">--}}
                        {{--<span aria-hidden="true">&times;</span>--}}
                    {{--</button>--}}
                    {{--<strong>--}}
                        {{--{!! session()->get('success') !!}--}}
                    {{--</strong>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@endif--}}
        <form class="form" role="form" id="form-setting" method="post" action="{{ route('admin-settings-update')  }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-5">
                    <div class="card card-default">
                        <div class="card-header">
                            <div class="card-title">
                                Site Logo And Favicon
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group form-group-default">
                                            <label>Logo</label>
                                            <input type="file" class="form-control" name="site_logo" id="site_logo">
                                        </div>

                                    </div>
                                    <div class="col-md-4 text-center">
                                        @if(isset($setting->site_logo))
                                            <img src="{{ asset($setting->site_logo)  }}" alt="" class="img-circle profile_img">
                                        @else
                                            <img src="#" alt="" class="img-circle profile_img">
                                        @endif
                                    </div>
                                    <div class="col-md-8 mt-2">
                                        <div class="form-group form-group-default">
                                            <label>Favicon</label>
                                            <input type="file" class="form-control" name="site_fav_logo" id="site_fav_logo">
                                        </div>
                                    </div>
                                    <div class="col-md-4 mt-2 text-center">
                                        @if(isset($setting->site_fav_logo))
                                            <img src="{{ asset($setting->site_fav_logo)  }}" alt="" class="img-circle-fav profile_fav_img">
                                        @else
                                            <img src="#" alt="" class="img-circle-fav profile_fav_img">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-default">
                        <div class="card-header">
                            <div class="card-title">
                                Social Links
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group form-group-default">
                                <label>Facebook</label>
                                <input type="text" class="form-control" name="facebook_link" value="{{ isset($setting->facebook_link) ? $setting->facebook_link: ''  }}">
                            </div>
                            <div class="form-group form-group-default">
                                <label>Twitter</label>
                                <input type="text" class="form-control" name="twitter_link" value="{{ isset($setting->twitter_link) ? $setting->twitter_link : ''  }}">
                            </div>
                            <div class="form-group form-group-default">
                                <label>Instagram</label>
                                <input type="text" class="form-control" name="instagram_link" value="{{ isset($setting->instagram_link) ? $setting->instagram_link : ''  }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="card card-default">
                        <div class="card-header ">
                            <div class="card-title">
                                Company Details
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group form-group-default required">
                                <label>Company Name</label>
                                <input type="text" class="form-control" name="company_name" id="company_name" required value="{{ isset($setting->company_name) ? $setting->company_name : ''  }}">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Company Email</label>
                                        <input type="text" name="company_email" class="form-control" id="company_email" required value="{{ isset($setting->company_email) ? $setting->company_email  : ''  }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required">
                                        <label>Company Number</label>
                                        <input type="text" name="company_number" class="form-control" id="company_number" required value="{{ isset($setting->company_mobile) ? $setting->company_mobile : ''  }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Pan Number</label>
                                <input type="text" name="pan_number" class="form-control" id="pan_number" value="{{ isset($setting->pan_number) ? $setting->pan_number : ''  }}">
                            </div>
                            <div class="form-group form-group-default">
                                <label>GST No</label>
                                <input type="text" name="gst_no" class="form-control" id="gst_no" value="{{ isset($setting->gst_no)  ?  $setting->gst_no : '' }}">
                            </div>
                            <div class="form-group">
                                <label>Address</label> <span class="text-danger">*</span>
                                <textarea name="company_address" id="company_address" rows="4" class="form-control" required>{{ isset($setting->company_address) ? $setting->company_address : ''    }}</textarea>
                            </div>
                            <div class="form-group form-group-default required">
                                <label>PinCode</label>
                                <input type="text" name="pin_code" class="form-control" id="pin_code" required value="{{ isset($setting->pin_code) ? $setting->pin_code : ''  }}">
                            </div>
                            <div class="form-group form-group-default required">
                                <label>City</label>
                                <input type="text" name="city" class="form-control" id="city" required value="{{ isset($setting->city) ? $setting->city : ''  }}">
                            </div>
                            <div class="form-group form-group-default required">
                                <label>State</label>
                                <input type="text" name="state" class="form-control" id="state" required value="{{ isset($setting->state) ? $setting->state : ''  }}">
                            </div>
                            <div class="form-group">
                                <label>Country</label> <span class="text-danger">*</span>
                                <select class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" name="country">
                                    <option value="">Select Country</option>
                                    @foreach($countries as $key => $country)
                                        <option value="{{ $key  }}" {{ isset($setting->country) ? $setting->country == $key ? 'selected' : '' : '' }}>{{ $country  }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group text-center">
                        <button class="btn btn-primary btn-md" type="submit">Update</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
@section('page-js')
    <script>
        $(document).ready(function(){
            $('#form-setting').validate();
            $('#site_logo').change(function (e) {
                e.preventDefault();
                const file = this.files[0];
                if (file){
                    let reader = new FileReader();
                    reader.onload = function(event){
                        $('.profile_img').attr('src', event.target.result);
                    };
                    reader.readAsDataURL(file);
                }
            });
            $('#site_fav_logo').change(function (e) {
                e.preventDefault();
                const file = this.files[0];
                if (file){
                    let reader = new FileReader();
                    reader.onload = function(event){
                        $('.profile_fav_img').attr('src', event.target.result);
                    };
                    reader.readAsDataURL(file);
                }
            });
            setTimeout(function (){
                $('.alert-message').empty();
            },2000);
        });
    </script>
@endsection
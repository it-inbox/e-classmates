@extends('admin.template.layout')
@section('title','Contact Inquiry')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Contact Inquiry:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Contact</th>
                            <th>Email</th>
                            <th>Message</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contactInquiries as $index => $contactInquiry)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $contactInquiry->name  }}</td>
                                <td>{{ $contactInquiry->contact  }}</td>
                                <td>{{ $contactInquiry->email   }}</td>
                                <td>{{ $contactInquiry->message }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-8">
            </div>
            <div class="col-md-4 text-right">
                @if ($contactInquiries->lastPage() > 1)
                    <ul class="pagination">
                        <li class="{{ ($contactInquiries->currentPage() == 1) ? ' disabled' : '' }}">
                            <a href="{{ $contactInquiries->url(1) }}" class="btn btn-default">Previous</a>
                        </li>
                        @for ($i = 1; $i <= $contactInquiries->lastPage(); $i++)
                            <li class="{{ ($contactInquiries->currentPage() == $i) ? ' active' : '' }}">
                                <a href="{{ $contactInquiries->url($i) }}" class="circle">{{ $i }}</a>
                            </li>
                        @endfor
                        <li class="{{ ($contactInquiries->currentPage() == $contactInquiries->lastPage()) ? ' disabled' : '' }}">
                            <a href="{{ $contactInquiries->url($contactInquiries->currentPage()+1) }}" class="btn btn-default">Next</a>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
    </div>
@endsection
@extends('admin.template.layout')
@section('title','Boards')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Boards:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <a href="{{ route('admin-boards-create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>   Add Board</a>
                    </div>
                </div>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Board</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($boards as $index => $board)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $board->name }}</td>
                            <td>
                                @if ($board->status =='1')
                                    <span class="badge badge-primary">Active</span>
                                @else
                                    <span class="badge badge-warning">In-Active</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('admin-boards-edit',$board->id) }}" class="btn btn-warning" title="Edit Board"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
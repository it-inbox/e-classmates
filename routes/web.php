<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProductController;


//Route::get('/',function(){
//    return view('welcome');
//});



Route::get('login',[LoginController::class,'index'])->name('login');
Route::get('auth',[LoginController::class,'auth'])->name('login-auth');
Route::get('/',[ProductController::class,'index'])->name('product');

Route::get('cart',[ProductController::class,'cart'])->name('view-cart');
Route::get('add-to-cart/{id}',[ProductController::class,'addToCart'])->name('add.to.cart');

Route::get('checkout',[ProductController::class,'checkout'])->name('checkout');

Route::get('place-order',[ProductController::class,'placeOrder'])->name('place-order');

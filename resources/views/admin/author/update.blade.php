@extends('admin.template.layout')
@section('title','Author')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Author:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{ route('author-list-update') }}">
                            @csrf
                            <input type="hidden" name="id" value="{{ $author->id }}">
                            <div class="form-group form-group-default">
                                <label for="author">Author<span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('author') is-invalid @enderror" id="author" name="author" value="{{ old('author', $author->author) }}">
                                @error('author')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="status">Status</label>
                                <div class="form-check form-check-inline">
                                    <input type="radio" name="status" id="radioInline1" value="1" {{ $author->status == 1 ? 'checked' : ''  }}>
                                    <label for="radioInline1">
                                        Active
                                    </label>
                                </div> <div class="form-check form-check-inline complete">
                                    <input type="radio" name="status" id="radioDisabled2" value="2" {{ $author->status == 2 ? 'checked' : ''  }}>
                                    <label for="radioDisabled2">
                                        In-active
                                    </label>
                                </div>
                            </div>


                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary btn-md">Update</button>
                                <a href="{{  route('author-list-view') }}" class="btn text-dark cancel-button">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
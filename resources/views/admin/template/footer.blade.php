<div class=" container-fluid  container-fixed-lg footer">
    <div class="copyright sm-text-center">
        <p class="small-text no-margin sm-pull-reset text-center">
            ©{{ date('Y')  }} All Rights Reserved.
            <span class="hint-text m-l-15 text-danger">{{ env('APP_NAME')  }}</span>
        </p>
        <div class="clearfix"></div>
    </div>
</div>
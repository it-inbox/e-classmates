<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    const ACTIVE = 1, INACTIVE = 2;
    const YES = 1, No = 2;
    const IN = 1, OUT = 2;

    protected $fillable = [
        'book_name','board','language','publisher','publication_year','category','sub_category','book_type','book_image','author','SKU','stock','price','distributor_price','qty','barcode','gst','featured','status'
    ];

    public function publishers(){
        return $this->belongsTo(Publisher::class,'publisher','id');
    }

    public function authors(){
        return $this->belongsTo(Author::class,'author','id');
    }

    public function boards(){
        return $this->belongsTo(Board::class,'board','id');
    }

    public function language(){
        return $this->belongsTo(Language::class,'language','id');
    }

    public function category(){
        return $this->belongsTo(Category::class,'category','id');
    }

    public function book_type(){
        return $this->belongsTo(BookType::class,'book_type','id');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        if (!empty($request->parent_id))
            $categories = Category::whereParentId($request->parent_id)->with(['parent'])->orderBy('id', 'desc')->paginate(30);
        else
            $categories = Category::whereNull('parent_id')->with(['parent'])->orderBy('id', 'desc')->paginate(30);

        return view('admin.category.index', ['categories' => $categories]);

    }
    public function create(Request $request)
    {
        if($request->isMethod('post'))
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:categories,name'
            ],[
                'name.unique' => 'Category Name is already exists, try different name'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            Category::create([
                'parent_id' => $request->parent_category_id ? $request->parent_category_id : null,
                'name' => $request->name,
                'slug' => \Str::slug($request->name, '-'),
            ]);

            return redirect()->route('admin-category-view')->with(['success' => 'New Category has been created..!!']);
        }

        return view('admin.category.create', [
            'categories' => Category::whereNull('parent_id')->get()
        ]);
    }
    public function update(Request $request)
    {
        if (!$category = Category::whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Category Request Id, Try Again']);

        if($request->isMethod('post'))
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:categories,name,' . $category->id,
                'status' => 'required',
            ], [
                'name.unique' => 'Category Name is already exists, try different name'
            ]);

            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $category->name =  $request->name;
            $category->status =  $request->status;
            $category->save();

            return redirect()->route('admin-category-view')->with(['success' => $category->name . ' Category Update Successfully']);

        }
        return view('admin.category.update',[
            'category' => $category
        ]);
    }

    public function apiGetCategories(Request $request)
    {
        if (!Category::whereNull('parent_id')->whereId($request->category_id)->exists())
            return response()->json(['status' => false, 'message' => 'Category not exists']);

        $categories = Category::selectRaw('id as value, name as label')->whereParentId($request->category_id)
            ->active()->get()->toArray();

        return response()->json(['status' => true,'categories' => $categories,]);
    }
}

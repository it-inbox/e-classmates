<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function index(){
        if(Session::get('admin')){
            return redirect()->route('admin-dashboard');
        }
        return view('admin.login');
    }

    public function Auth(Request $request){
        $validator = Validator::make([
            'username' => 'Required',
            'password' => 'Required'
        ],[
            'username.required' => 'Email Address is Required',
            'password.required' => 'Password is Required'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $admin = Admin::where('email',$request->username)->first();
        if(!$admin){
            return redirect()->back()->with(['errors' => 'Invalid Email And Password']);
        }else{
            if(Hash::check($request->password,$admin->password)){
                Admin::whereId($admin->id)->update([
                     'last_logged_in_at' => Carbon::now()
                 ]);
                Session::put('admin',$admin);
                return redirect()->route('admin-dashboard');
            }else{
                return redirect()->back()->with(['errors' => 'Invalid Email And Password']);
            }
        }
    }

    public function profile(Request $request)
    {
        if($request->isMethod('post')){

            $admin = Admin::find(Session::get('admin')->id);

            $this->validate($request, [
                'name' => 'required',
                'password' => 'min:6|nullable'
            ],[
                'name.required' => 'Name is Required',
                'password.min' => 'Password length should be 6 or more',
            ]);

            $admin->username = $request->name;
            $admin->password = !empty($request->password) ? Hash::make($request->password) : $admin->password;
            $admin->save();

            Session::put('admin', $admin);

            return redirect()->route('admin-dashboard')->with(['success' => 'Profile has been updated..!!']);
        }

        return view('admin.manager.profile');
    }

    public function adminUserAccountAccess(Request $request){

        if(!$user = User::whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid User Details for View']);

        return redirect()->route('user-admin-view-access', ['id' => $user->id, 'token' => \Session::get('admin')->token]);
    }

    public function logout(){
        Session::forget('admin');
        return redirect()->route('admin-login');

    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;

    const ACTIVE = 1, INACTIVE = 2;

    protected  $fillable = [
        'country_id','country_code','state_code','name','slug'
    ];
}

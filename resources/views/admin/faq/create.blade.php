@extends('admin.template.layout')
@section('title','FAQ')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,FAQ:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{ route('faq-list-store') }}">
                            @csrf
                            <div class="form-group form-group-default">
                                <label for="question">Question<span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('question') is-invalid @enderror" id="question" name="question" value="{{ old('question') }}">
                                @error('question')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group form-group-default">
                                <label for="answer">Answer<span class="text-danger">*</span></label>
                                <textarea class="form-control @error('question') is-invalid @enderror" id="answer" name="answer" rows="4" size="50">{{ old('answer') }}</textarea>
                                @error('answer')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <div class="form-check form-check-inline">
                                    <input type="radio" name="status" id="radioInline1" value="1">
                                    <label for="radioInline1">
                                        Active
                                    </label>
                                </div> <div class="form-check form-check-inline complete">
                                    <input type="radio" name="status" id="radioDisabled2" value="2">
                                    <label for="radioDisabled2">
                                        In-active
                                    </label>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary btn-md">Submit</button>
                                <a href="{{  route('faq-list-view') }}" class="btn text-dark cancel-button">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
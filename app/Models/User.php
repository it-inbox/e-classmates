<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable,SoftDeletes;

    const ACTIVE = 1, INACTIVE = 2;
    const MALE = 1, FEMALE = 2, OTHER = 3;

    protected $fillable = [
        'profile_photo','full_name', 'username','first_name','middle_name','last_name','email','password','show_password','mobile','country','gender','date_of_birth','address', 'role_id', 'status','lang','channel_type','marital_status','age','anniversary_date','office_address'
    ];

    public function display_name(){

        if($this->full_name != ''){
            return $this->full_name;
        }else if($this->first_name != '' || $this->middle_name != '' || $this->last_name != ''){
            return $this->first_name.' '.$this->middle_name.' '.$this->last_name;
        }else{
            return $this->username;
        }
    }

    public function user_company(){
        return $this->hasOne(UserCompany::class,'user_id','id');
    }
}

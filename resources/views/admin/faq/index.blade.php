@extends('admin.template.layout')
@section('title','Publisher')
@section('page-content')
@breadcrumb(Dashboard:admin-dashboard,Publisher:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 text-right mb-3">
                        <a href="{{ route('faq-list-create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add Faq</a>
                    </div>
                </div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Question</th>
                        <th>Answer</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($faqs as $index => $faq)

                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $faq -> question }}</td>
                            <td>{{ $faq -> answer }}</td>
                            <td>
                                @if($faq->status == '1')
                                    <span class="badge badge-success">Active</span>
                                @else
                                    <span class="badge badge-danger">Inactive</span>
                                @endif
                            </td>
                            
                           
                            <td>
                                <a href="{{ route('faq-list-edit', $faq->id) }}" class="btn btn-warning" title="Edit Publisher"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                  
                </table>
            </div>
        </div>
    </div>
@endsection
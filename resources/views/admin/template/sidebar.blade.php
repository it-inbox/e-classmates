@php
    $request = request()->segments(1);
    $setting = \App\Models\Setting::orderBy('id','desc')->first();
@endphp
<nav class="page-sidebar" data-pages="sidebar">
    <div class="sidebar-header text-center">
        @if(isset($setting->site_logo) && !empty($setting->site_logo))
            <img src="{{ asset($setting->site_logo) }}" alt="logo" class="brand" data-src="{{ asset($setting->site_logo) }}" data-src-retina="{{ asset('assets/img/logo_2x.png')  }}" width="100">
            @else
            <img src="{{ asset('admin-assets/company/logo.png') }}" alt="logo" class="brand" data-src="{{ asset('admin-assets/company/logo.png') }}" data-src-retina="{{ asset('assets/img/logo_2x.png')  }}" width="100">
        @endif
    </div>
    <div class="sidebar-menu">
        <ul class="menu-items">
            <li class="m-t-10 {{ $request[1] == 'dashboard' ? 'active' : ''  }}">
                <a href="{{ route('admin-dashboard')  }}" class="detailed">
                    <span class="title">Dashboard</span>
                </a>
                <span class="icon-thumbnail"><i data-feather="home"></i></span>
            </li>
            <li class="{{ $request[1] == 'user' ? 'active' : ''  }}">
                <a href="{{ route('user-list-view')  }}"><span class="title">Users</span></a>
                <span class="icon-thumbnail"><i data-feather="user"></i></span>
            </li>
            <li class="{{ $request[1] == 'publisher' ? 'active' : ''  }}">
                <a href="{{ route('publisher-list-view')  }}"><span class="title">Publishers</span></a>
                <span class="icon-thumbnail">P</span>
            </li>
            <li class="{{ $request[1] == 'author' ? 'active' : ''  }}">
                <a href="{{ route('author-list-view')  }}"><span class="title">Authors</span></a>
                <span class="icon-thumbnail">A</span>
            </li>
            <li class="{{ $request[1] == 'boards' ? 'active' : ''  }}">
                <a href="{{ route('admin-boards-view')  }}"><span class="title">Boards</span></a>
                <span class="icon-thumbnail">B</span>
            </li>
            <li class="{{ $request[1] == 'category' ? 'active' : ''  }}">
                <a href="{{ route('admin-category-view')  }}"><span class="title">Category</span></a>
                <span class="icon-thumbnail">C</span>
            </li>
            @php
                $block = "";
                $active = "";
                $open = "";
                if($request[1] == 'books' || $request[1] == 'book_types' || $request[1] == 'book_languages'){
                    $block = "d-block";
                    $active = "active";
                    $open = "open";
                }else{
                    $block = "";
                }
            @endphp
            <li class="{{ $open.' '.$active }}">
                <a href="javascript:void(0);"><span class="title">Books</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i data-feather="file-text"></i></span>
                <ul class="sub-menu {{ $block }}">
                    <li class="{{ $request[1] == 'books' ? 'active' : '' }}">
                        <a href="{{ route('admin-books-list-view')  }}">List</a>
                        <span class="icon-thumbnail"><i data-feather="list"></i></span>
                    </li>
                    <li class="{{ $request[1] == 'book_types' ? 'active' : '' }}">
                        <a href="{{  route('book_types_list')  }}">Types</a>
                        <span class="icon-thumbnail">T</span>
                    </li>
                    <li class="{{ $request[1] == 'book_languages' ? 'active' : '' }}">
                        <a href="{{ route('book_languages_list_view') }}">Language</a>
                        <span class="icon-thumbnail">L</span>
                    </li>
                </ul>
            </li>
            <li class="{{ $request[1] == 'banner' ? 'active' : ''  }}">
                <a href="{{ route('admin-banner-view')  }}"><span class="title">Banners</span></a>
                <span class="icon-thumbnail">B</span>
            </li>
            <li class="{{ $request[1] == 'faq' ? 'active' : ''  }}">
                <a href="{{ route('faq-list-view')  }}"><span class="title">FAQ</span></a>
                <span class="icon-thumbnail">F</span>
            </li>
            <li class="{{ $request[1] == 'cms' ? 'active' : ''  }}">
                <a href="{{ route('admin-cms')  }}"><span class="title">CMS Pages</span></a>
                <span class="icon-thumbnail">C</span>
            </li>
            <li class="{{ $request[1] == 'contact-inquiry' ? 'active' : ''  }}">
                <a href="{{ route('admin-contact-inquiry')  }}"><span class="title">Contact Inquiry</span></a>
                <span class="icon-thumbnail">C</span>
            </li>
            <li class="{{ $request[1] == 'settings' ? 'active' : ''  }}">
                <a href="{{ route('admin-settings')  }}"><span class="title">Settings</span></a>
                <span class="icon-thumbnail"><i data-feather="settings"></i></span>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
</nav>
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Language;

class BookLangaugeController extends Controller
{
    public function index(){
        $languages = Language::orderBy('id','desc')->get();

        return view('admin.books.language.index',[
            'languages' => $languages
        ]);
    }

    public function create(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'name' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            Language::create([
                'name' => $request->name,
                'slug' =>  \Str::slug($request->name,'-'),
                'status' => Language::ACTIVE
            ]);
            $languageActivity = Activity::all()->last();
            return redirect()->route('languages_list_view')->with(['success' => 'Language Added Successfully!']);
        }
        return view('admin.books.language.create');
    }

    public function update(Request $request,$id){
        $language = Language::where('id',$id)->first();
        $histories = Activity::where('subject_id', $id)->orderBy('id', 'desc')->get();
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'name' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $language->name =  $request->name;
            $language->slug =  \Str::slug($request->name,'-');
            $language->status = $request->status;
            $language->save();

            $activity = Activity::all()->last();
            return redirect()->route('languages_list_view')->with(['success' => 'Language updated Successfully!']);
        }

        return view('admin.books.language.update',[
            'language' => $language,
            'histories' => $histories
        ]);
    }
}

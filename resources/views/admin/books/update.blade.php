@extends('admin.template.layout')
@section('title','Update')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Books:admin-books-list-view,Update:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="{{ route('admin-books-update')  }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{ $book->id  }}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <label for="">Book Name</label>
                                    <input type="text" class="form-control" name="book_name" value="{{ $book->book_name  }}">
                                </div>
                                <div class="form-group form-group-default">
                                    <label for="">Board</label>
                                    <select class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" name="board">
                                        <option value="">Select</option>
                                        @foreach($boards as $key => $board)
                                            <option value="{{ $board->id  }}" {{ $book->board == $board->id ? 'selected' : ''  }}>{{ $board->name  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group form-group-default">
                                    <label for="">language</label>
                                    <select class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" name="language">
                                        <option value="">Select</option>
                                        @foreach($languages as $language)
                                            <option value="{{  $language->id  }}" {{ $book->language == $language->id ? 'selected' : ''  }}>{{  $language->name  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group form-group-default">
                                    <label for="">Publisher</label>
                                    <select class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" name="publisher">
                                        <option value="">Select</option>
                                        @foreach($publishers as $publisher)
                                            <option value="{{  $publisher->id }}" {{ $book->publisher == $publisher->id ? 'selected' : ''  }}>{{ $publisher->name  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group form-group-default">
                                    <label for="">Publication Year</label>
                                    <input type="date" name="publication_year" id="publication_year" class="form-control" value="{{ date('Y')  }}">
                                </div>
                                <div class="form-group form-group-default">
                                    <label for="">Category</label>
                                    <select class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" name="category">
                                    </select>
                                </div>
                                <div class="form-group form-group-default">
                                    <label for="">Sub Category</label>
                                    <select class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" name="sub_category">
                                    </select>
                                </div>
                                <div class="form-group form-group-default">
                                    <label for="">Book Type</label>
                                    <select class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" name="book_type">
                                    </select>
                                </div>
                                <div class="form-group form-group-default">
                                    <label for="">Author</label>
                                    <select class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" name="author">
                                        <option value="">Select</option>
                                        @foreach($authors as $author)
                                            <option value="{{ $author->id  }}" {{ $book->author == $author->id ? 'selected' : ''  }}>{{ $author->author  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group form-group-default">
                                    <label for="">SKU</label>
                                    <input type="text" name="sku" id="sku" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Stock</label>
                                    <input type="radio" name="stock" id="stock" value="in-stock" {{ $book->stock == \App\Models\Book::IN ? 'checked' : ''  }}> In Stock
                                    <input type="radio" name="stock" id="stock" value="out-of-stock" {{ $book->stock == \App\Models\Book::OUT ? 'checked' : ''  }}> Out Of Stock
                                </div>
                                <div class="form-group form-group-default">
                                    <label for="">Price</label>
                                    <input type="text" name="price" id="price" class="form-control" value="{{ $book->price  }}">
                                </div>
                                <div class="form-group form-group-default">
                                    <label for="">Distributor Price</label>
                                    <input type="text" name="distributor_price" id="distributor_price" class="form-control">
                                </div>
                                <div class="form-group form-group-default">
                                    <label for="">Qty</label>
                                    <input type="text" name="qty" id="qty" class="form-control">
                                </div>
                                <div class="form-group form-group-default">
                                    <label for="">Barcode</label>
                                    <input type="text" name="barcode" id="barcode" class="form-control">
                                </div>
                                <div class="form-group form-group-default">
                                    <label for="">GST</label>
                                    <input type="text" name="gst" id="gst" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Featured</label>
                                    <input type="radio" name="featured" id="featured" value="yes" {{ $book->featured == \App\Models\Book::YES ? 'checked' : ''  }}> Yes
                                    <input type="radio" name="featured" id="featured" value="no" {{ $book->featured == \App\Models\Book::No ? 'checked' : ''  }}> No
                                </div>
                                <div class="form-group">
                                    <label for="">Status</label>
                                    <input type="radio" name="status" id="status" value="active" {{ $book->status == \App\Models\Book::ACTIVE ? 'checked' : ''  }}> Active
                                    <input type="radio" name="status" id="status" value="in-active" {{ $book->status == \App\Models\Book::INACTIVE ? 'checked' : ''  }}> In-active
                                </div>
                            </div>
                            <div class="col-md-6 text-center">
                                <div class="dz-default dlab-message upload-img mb-3">
                                    <div class="fallback">
                                        <img class="img-circle profile_img" src="{{ asset($book->book_image)  }}" alt="" />
                                        <input name="book_image" type="file" id="book_image">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn btn-primary btn-md text-center" type="submit">Update</button>
                                <a href="{{  route('admin-books-list-view') }}" class="btn text-dark cancel-button">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page-js')
    <script>
        $(document).ready(function(){
            $('#book_image').change(function (e) {
                e.preventDefault();
                const file = this.files[0];
                if (file){
                    let reader = new FileReader();
                    reader.onload = function(event){
                        $('.profile_img').attr('src', event.target.result);
                    };
                    reader.readAsDataURL(file);
                }
            })
        });
    </script>
@endsection
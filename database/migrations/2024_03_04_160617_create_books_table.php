<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('book_name');
            $table->tinyInteger('board')->default(0)->nullable();
            $table->tinyInteger('language')->default(0)->nullable();
            $table->tinyInteger('publisher')->default(0)->nullable();
            $table->year('publication_year')->nullable();
            $table->tinyInteger('category')->default(0)->nullable();
            $table->tinyInteger('sub_category')->default(0)->nullable();
            $table->tinyInteger('book_type')->default(0)->nullable();
            $table->string('book_image')->nullable();
            $table->tinyInteger('author')->nullable();
            $table->string('SKU')->nullable();
            $table->tinyInteger('stock')->comment('1:In Stock,2:Out Of Stock')->default(0);
            $table->float('price',10,2)->nullable();
            $table->float('distributor_price',10,2)->nullable();
            $table->integer('qty')->default(0);
            $table->string('barcode')->nullable();
            $table->string('gst')->comment('Json: percentage & hsn/sac code');
            $table->tinyInteger('featured')->comment('1:Yes,2:No')->default(0);
            $table->tinyInteger('status')->comment('1:Active,2:In-active')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}

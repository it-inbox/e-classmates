<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BannerController extends Controller
{
    public function index(){
        $banners = Banner::orderBy('id','desc')->get();

        return view('admin.banners.index',[
            'banners' => $banners
        ]);
    }

    public function create(){
        return view('admin.banners.create');
    }

    public function store(Request $request){
        if($request->isMethod('post'))
        {
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'image' => 'mimes:jpeg,jpg,png|max:5000'
            ],[
                'name.required' => 'Image Title or Name is required',
                'image.mimes' => 'Image must have .jpg,jpeg and png type',
                'image.max' => 'Upload an image less then 5MB'
            ]);

            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            if($request->image)
            {
                if($request->hasFile('image') && $request->file('image')->isValid())
                {
                    $image = $request->file('image');
                    $destinationPath = '/uploads/banners/';
                    $imageName = rand().time().'.'.$image->getClientOriginalExtension();
                    $image->move(public_path().$destinationPath, $imageName);

                    Banner::create([
                        'name' => $request->name,
                        'image' => $destinationPath.$imageName,
                        'type' => Banner::BANNER
                    ]);
                }
            }
            return redirect()->route('admin-banner-view')->with('success','New Banner has been Uploaded.');
        }
    }

    public function edit($id){
        return view('admin.banners.update',[
            'banner' => Banner::where('id',$id)->first()
        ]);
    }

    public function update(Request $request){

        $banner = Banner::where('id',$request->id)->first();

        if($request->isMethod('post'))
        {
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'image' => 'mimes:jpeg,jpg,png|max:5000'
            ],[
                'name.required' => 'Image Title or Name is required',
                'image.mimes' => 'Image must have .jpg,jpeg and png type',
                'image.max' => 'Upload an image less then 5MB'
            ]);

            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            if($request->image)
            {
                if($request->hasFile('image') && $request->file('image')->isValid())
                {
                    $image = $request->file('image');
                    $destinationPath = '/uploads/banners/';
                    $imageName = rand().time().'.'.$image->getClientOriginalExtension();
                    $image->move(public_path().$destinationPath, $imageName);
                    $banner->image = $destinationPath.$imageName;
                }
            }

            $banner->name = $request->name;
            $banner->status = $request->status;
            $banner->save();

            return redirect()->route('admin-banner-view')->with(['success' => 'Banner has been Uploaded.']);
        }
    }
}

@extends('admin.template.layout')
@section('title','Create')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Banners:admin-banner-view,Create:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('admin-banner-store')  }}" method="post" role="form" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 offset-md-3 offset-lg-3">
                            <div class="form-group form-group-default">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}" required>
                            </div>
                            <div class="form-group">
                                <label>Select Banner image</label>
                                <input type="file" name="image" id="image">
                                <p><br>1. Upload File Size Not More than 2 MB.</p>
                                <p>2. File Upload Before Getting Smaller Size From <a href="http://tinypng.com" target="_blank">Tinypng.com</a><br/></p>
                                <p class="text-danger">3. For Responsive slider upload image size in 1400*600 pixels</p>
                            </div>
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary btn-cons m-b-10"><i class="fa fa-cloud-upload"></i> <span class="bold">Upload</span>
                                </button>
                            </div>

                        </div>


                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;

    const ACTIVE = 1 ,INACTIVE = 2;
    const BANNER = 1, OTHER = 2, VIDEO = 3;

    protected  $fillable = [
        'name' , 'image', 'type' ,'status'
    ];

    public function scopeActive($q){
        return $q->where('status',self::ACTIVE);
    }

    public function scopeBanner($q){
        return $q->where('type',self::BANNER);
    }

    public function scopeOther($q){
        return $q->where('type',self::OTHER);
    }

    public function scopeVideo($q){
        return $q->where('type',self::VIDEO);
    }
}

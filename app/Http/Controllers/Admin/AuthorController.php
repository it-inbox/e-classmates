<?php

namespace App\Http\Controllers\Admin;

use App\Models\Author;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AuthorController extends Controller
{
    public function index(){

        $authors = Author::all();
        return view('admin.author.index', compact('authors'));
    }

    public function create(){
        
        return view('admin.author.create');
    }

    public function store(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'author' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            Author::create([
                'author' => $request->author,
                
                'status' => Author::ACTIVE
            ]);

            return redirect()->route('author-list-view')->with('success','Author Created Successfully');
        }
    }

    public function edit($id){
        return view('admin.author.update',[
            'author' => Author::where('id',$id)->first()
        ]);
    }

    public function update(Request $request)
    {
        $author = Author::find($request->id);
    
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'author' => 'required'
            ]);
    
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
    
            $author->author = $request->author;
            $author->status = $request->status;
            $author->save();
    
            return redirect()->route('author-list-view')->with('success', 'Author Updated Successfully'); // Fix the route name
        }
    }
    



}

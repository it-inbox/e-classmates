@extends('admin.template.layout')
@section('title','Book Languages')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Book Languages:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <a href="{{ route('book_languages_create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>   Add Language</a>
                    </div>
                </div>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($languages as $index => $language)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $language->name }}</td>
                            <td>
                                @if ($language->status =='1')
                                    <span class="badge badge-primary">Active</span>
                                @else
                                    <span class="badge badge-danger">In-Active</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('book_languages_update',$language->id) }}" class="btn btn-warning" title="Edit Board"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;

    const  ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'author','status'
    ];

    public function book(){
        return $this->hasOne(Book::class);
    }
}


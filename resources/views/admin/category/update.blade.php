@extends('admin.template.layout')
@section('title', 'Update Category')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Category:admin-category-view,Update:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('admin-category-update',$category->id)  }}" method="post" role="form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group form-group-default">
                                <label>Parent Category</label>
                                <input type="text" name="name" class="form-control" value="{{ $category->parent_id ? $category->parent->name : 'N.A' }}" readonly>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Category Name</label>
                                <input type="text" name="name" class="form-control" value="{{ old('name',$category->name) }}">
                                @error('name')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <div class="form-check form-check-inline">
                                    <input type="radio" name="status" id="radioInline1" value="1" {{ $category->status == 1 ? 'checked' : ''  }}>
                                    <label for="radioInline1">
                                        Active
                                    </label>
                                </div> <div class="form-check form-check-inline complete">
                                    <input type="radio" name="status" id="radioDisabled2" value="2" {{ $category->status == 2 ? 'checked' : ''  }}>
                                    <label for="radioDisabled2">
                                        In-active
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-danger btn-md    " type="submit"> Update </button>
                                    <a href="{{  route('admin-category-view') }}" class="btn text-dark cancel-button">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
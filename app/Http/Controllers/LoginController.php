<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function index(){
        return view('login');
    }

    public function auth(Request $request){
        $validator = Validator::make($request->all(),[
            'email' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }

        $user = User::where('email',$request->email)->first();
        if(!$user)
            return redirect()->back()->with(['errors' => 'Invalid Email and Password']);
        else
            if(Hash::check($request->password,$user->password)){
                Session::put('front_user',$user);
                return redirect()->route('/');
            }else{
                return redirect()->back()->with(['errors' => 'Invalid Email And Password']);
            }
    }
}

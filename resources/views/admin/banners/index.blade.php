@extends('admin.template.layout')
@section('title','Banner')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Banner:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-12 pull-right">
                            <a href="{{ route('admin-banner-create') }}" class="btn btn-primary btn-md pull-right"> Create New </a>
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($banners as $index => $banner)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                <td>{{ $banner->name }}</td>
                                <td>
                                    @if($banner->type == \App\Models\Banner::BANNER)
                                        <span class="badge badge-pill">Banner</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($banner->status == 1)
                                        <span class="badge badge-primary"> Active</span>
                                    @else
                                        <span class="badge badge-danger"> In Active</span>
                                    @endif
                                </td>
                                <td><a href="{{ asset($banner->image) }}" target="_blank" class="btn-md btn btn-primary"> View Image </a></td>
                                <td>
                                    <a href="{{ route('admin-banner-edit',$banner->id) }}" class="btn btn-warning btn-md"> <i class="fa fa-pencil"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
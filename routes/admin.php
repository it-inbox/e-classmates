<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\Admin\MasterController;
use App\Http\Controllers\Admin\PublisherController;
use App\Http\Controllers\Admin\AuthorController;
use App\Http\Controllers\Admin\BoardController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\CmsController;
use App\Http\Controllers\Admin\BooktypesController;
use App\http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ContactInquiryController;
use App\Http\Controllers\Admin\BookController;
use App\Http\Controllers\Admin\BannerController;

Route::prefix('admin')->group(function () {
    Route::get('/',function(){
        return redirect()->route('admin-login');
    });
    Route::get('login',[LoginController::class,'index'])->name('admin-login');
    Route::post('auth',[LoginController::class,'Auth'])->name('admin-auth');

    Route::group(['middleware' => 'admin.auth'],function(){
        Route::get('dashboard',[DashboardController::class,'index'])->name('admin-dashboard');

        Route::group(['prefix' => 'manager'], function () {
            Route::get('profile', [LoginController::class,'profile'])->name('admin-manager-profile');
            Route::post('profile', [LoginController::class,'profile'])->name('admin-manager-profile');
        });

        Route::group(['prefix' => 'user'],function (){
            Route::get('/',[UserController::class,'index'])->name('user-list-view');
            Route::get('create',[UserController::class,'create'])->name('user-list-create');
            Route::post('store',[UserController::class,'store'])->name('user-list-store');
            Route::get('edit_user/{id}',[UserController::class,'edit'])->name('edit_user');
            Route::post('user-update',[UserController::class,'update'])->name('user-update');
            Route::get('delete_user/{id}',[UserController::class,'deleteUser'])->name('delete_user');

            Route::get('admin-user-account-access/{id}',[LoginController::class,'adminUserAccountAccess'])->name('admin-user-account-access');
        });

        Route::group(['prefix' => 'publisher'],function(){
            Route::get('/',[PublisherController::class,'index'])->name('publisher-list-view');
            Route::get('create',[PublisherController::class,'create'])->name('publisher-list-create');
            Route::post('store',[PublisherController::class,'store'])->name('publisher-list-store');
            Route::get('update/{id}',[PublisherController::class,'edit'])->name('publisher-list-edit');
            Route::post('update',[PublisherController::class,'update'])->name('publisher-list-update');
        });

        Route::group(['prefix' => 'author'],function(){
            Route::get('/',[AuthorController::class,'index'])->name('author-list-view');
            Route::get('create',[AuthorController::class,'create'])->name('author-list-create');
            Route::post('store',[AuthorController::class,'store'])->name('author-list-store');
            Route::get('update/{id}',[AuthorController::class,'edit'])->name('author-list-edit');
            Route::post('update',[AuthorController::class,'update'])->name('author-list-update');
        });

        Route::group(['prefix' => 'boards'],function(){
            Route::get('/',[BoardController::class,'index'])->name('admin-boards-view');
            Route::get('create',[BoardController::class,'create'])->name('admin-boards-create');
            Route::post('store',[BoardController::class,'store'])->name('admin-boards-store');
            Route::get('edit/{id}',[BoardController::class,'edit'])->name('admin-boards-edit');
            Route::post('update',[BoardController::class,'update'])->name('admin-boards-update');
        });


        Route::group(['prefix' => 'faq'],function(){
            Route::get('/',[FaqController::class,'index'])->name('faq-list-view');
            Route::get('create',[FaqController::class,'create'])->name('faq-list-create');
            Route::post('store',[FaqController::class,'store'])->name('faq-list-store');
            Route::get('update/{id}',[FaqController::class,'edit'])->name('faq-list-edit');
            Route::post('update',[FaqController::class,'update'])->name('faq-list-update');

        });

        Route::group(['prefix' => 'cms'], function () {
            Route::get('/', [CmsController::class, 'index'])->name('admin-cms');
            Route::get('edit/{id}',[CmsController::class,'edit'])->name('admin-cms-edit');
            Route::post('update',[CmsController::class,'update'])->name('admin-cms-update');
        });


        Route::group(['prefix' => 'book_types'], function () {
            Route::get('/', [BooktypesController::class, 'index'])->name('book_types_list');
            Route::get('/create', [BooktypesController::class, 'create'])->name('book_types_create');
            Route::post('/store', [BooktypesController::class, 'store'])->name('book_types_store');
            Route::get('edit/{id}',[BooktypesController::class,'edit'])->name('book_types_edit');
            Route::post('update',[BooktypesController::class,'update'])->name('book_types_update');
        });

        Route::group(['prefix' => 'category'], function () {
            Route::get('/', [CategoryController::class,'index'])->name('admin-category-view');
            Route::get('create', [CategoryController::class,'create'])->name('admin-category-create');
            Route::post('create', [CategoryController::class,'create'])->name('admin-category-create');
            Route::get('update/{id}', [CategoryController::class,'update'])->name('admin-category-update');
            Route::post('update/{id}', [CategoryController::class,'update'])->name('admin-category-update');
            Route::get('get-children', [CategoryController::class,'apiGetCategories'])->name('admin-api-category-get-children');
        });

        Route::group(['prefix' => 'contact-inquiry'],function(){
            Route::get('/',[ContactInquiryController::class,'index'])->name('admin-contact-inquiry');
        });

        Route::group(['prefix' => 'books'],function(){
            Route::get('/',[BookController::class,'index'])->name('admin-books-list-view');
            Route::get('create',[BookController::class,'create'])->name('admin-books-create');
            Route::post('store',[BookController::class,'store'])->name('admin-books-store');
            Route::get('edit/{id}',[BookController::class,'edit'])->name('admin-books-edit');
            Route::post('update',[BookController::class,'update'])->name('admin-books-update');
        });

        Route::group(['prefix' => 'banner'],function(){
            Route::get('/',[BannerController::class,'index'])->name('admin-banner-view');
            Route::get('create',[BannerController::class,'create'])->name('admin-banner-create');
            Route::post('store',[BannerController::class,'store'])->name('admin-banner-store');
            Route::get('edit/{id}',[BannerController::class,'edit'])->name('admin-banner-edit');
            Route::post('update',[BannerController::class,'update'])->name('admin-banner-update');
        });


        Route::group(['prefix' => 'settings'],function(){
            Route::get('/',[SettingController::class,'index'])->name('admin-settings');
            Route::post('update',[SettingController::class,'update'])->name('admin-settings-update');
        });


        Route::group(['prefix' =>'currencies'],function(){
            Route::get('/',[MasterController::class,'currencyIndex'])->name('currencies_list_view');
            Route::get('create',[MasterController::class,'currencyCreate'])->name('currencies_create');
            Route::post('create',[MasterController::class,'currencyCreate'])->name('currencies_create');
            Route::get('update/{id}',[MasterController::class,'currencyUpdate'])->name('currencies_update');
            Route::post('update/{id}',[MasterController::class,'currencyUpdate'])->name('currencies_update');
        });
        Route::group(['prefix' =>'book_languages'],function(){
            Route::get('/',[MasterController::class,'languageIndex'])->name('book_languages_list_view');
            Route::get('create',[MasterController::class,'languageCreate'])->name('book_languages_create');
            Route::post('create',[MasterController::class,'languageCreate'])->name('book_languages_create');
            Route::get('update/{id}',[MasterController::class,'languageUpdate'])->name('book_languages_update');
            Route::post('update/{id}',[MasterController::class,'languageUpdate'])->name('book_languages_update');
        });
    });
    Route::get('logout',[LoginController::class,'logout'])->name('admin-logout');
});

 
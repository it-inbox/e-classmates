@extends('admin.template.layout')
@section('title', 'Edit FAQ')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Faq:faq-list-view,Update:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('book_types_update') }}" method="post" role="form">
                            @csrf
                            <input type="hidden" name="id" value="{{ $books->id  }}">
                            <div class="form-group form-group-default">
                                <label for="name">name<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name',$books->name) }}">
                                @error('name')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <div class="form-check form-check-inline">
                                    <input type="radio" name="status" id="radioInline1" value="1" {{ $books->status == 1 ? 'checked' : ''  }}>
                                    <label for="radioInline1">
                                        Active
                                    </label>
                                </div> <div class="form-check form-check-inline complete">
                                    <input type="radio" name="status" id="radioDisabled2" value="2" {{ $books->status == 2 ? 'checked' : ''  }}>
                                    <label for="radioDisabled2">
                                        In-active
                                    </label>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="row">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-primary btn-md">Update</button>
                                    <a href="{{  route('book_types_list') }}" class="btn text-dark cancel-button">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@extends('admin.template.layout')
@section('title','Author')
@section('page-content')
@breadcrumb(Dashboard:admin-dashboard,Author:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 text-right mb-3">
                        <a href="{{ route('author-list-create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add Author</a>
                    </div>
                </div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Author</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                            @foreach($authors as $index => $author)
                                <tr>
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ $author->author }}</td>
                                    <td>
                                        @if ($author->status =='1')
                                            <span class="badge badge-primary">Active</span>
                                        @else
                                            <span class="badge badge-warning">In-Active</span>
                                        @endif
                                    </td>
                                    <td>
                                    <a href="{{ route('author-list-edit',$author->id) }}" class="btn btn-warning" title="Edit Publisher"><i class="fa fa-pencil"></i></a>

                                    </td>
                                </tr>
                            @endforeach

                    </tbody>

            
                </table>
            </div>
        </div>
    </div>
@endsection
@extends('admin.template.layout')
@section('title','Books')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Books:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-right">
                            <a href="{{  route('admin-books-create') }}" class="btn btn-primary">+ Add</a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Author</th>
                            <th>Publisher</th>
                            <th>Publication Year</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($books as $index => $book)
                            <tr>
                                <td>{{ $index + 1  }}</td>
                                <td><img src="{{ asset($book->book_image)  }}" alt="" width="50px" height="50px"></td>
                                <td>{{ $book->book_name  }}</td>
                                <td>{{ (isset($book->author) ? $book->authors->author : '')  }}</td>
                                <td>{{ $book->publishers->name  }}</td>
                                <td>{{ $book->publication_year }}</td>
                                <td>
                                    <a href="{{ route('admin-books-edit',$book->id)  }}" class="btn btn-warning btn-md"><i class="fa fa-pencil"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
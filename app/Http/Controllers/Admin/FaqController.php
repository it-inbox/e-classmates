<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Faq;
use Illuminate\Support\Facades\Validator;
use Spatie\Activitylog\Models\Activity;

class FaqController extends Controller
{
    public function index()
    {
        $faqs = Faq::all();
        $faqs = Faq::orderBy('id', 'desc')->paginate(5);
        return view('admin.faq.index', compact('faqs'));
    }

    public function create(){
        return view('admin.faq.create');
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'question' => 'required|string|max:100',
            'answer' => 'required|string|max:250'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        Faq::create([
            'question' => $request->question,
            'answer' => $request->answer,
            'status' => $request->status
        ]);

        // $faqActivity = Activity::all()->last();        

        return redirect()->route('faq-list-view')->with('success', 'FAQ added successfully');
    }


    public function edit($id)
    {
        $faq = Faq::findOrFail($id);
        //$histories = Activity::where('subject_id',$id)->orderBy('id','desc')->get();
        return view('admin.faq.update', compact('faq'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'question' => 'required',
            'answer' => 'required'
        ]);
        $faq = Faq::where('id',$request->id)->first();
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->status = ($request->status == '1'  ? Faq::ACTIVE : Faq::INACTIVE);
        $faq->save();

        //$activity = Activity::all()->last();

        return redirect()->route('faq-list-view')->with('success', 'FAQ updated successfully');
    }

    public function destroy($id)
    {
        $faq = Faq::withTrashed()->findOrFail($id);
        $faq->delete();
        return redirect()->route('faq-list-view')->with('success', 'FAQ marked as deleted successfully');
    }

    public function view($id)
    {
        $faq = Faq::findOrFail($id);
        return view('admin.faq.view', compact('faq'));
    }


}



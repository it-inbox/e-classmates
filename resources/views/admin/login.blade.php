<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Admin Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="{{ asset('admin-assets/ico/60.png')  }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('admin-assets/ico/76.png')  }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('admin-assets/ico/120.png')  }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{  asset('admin-assets/ico/152.png')  }}">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="Meet pages - The simplest and fastest way to build web UI for your dashboard or app." name="description" />
    <meta content="Ace" name="author" />
    <link href="{{ asset('admin-assets/plugins/pace/pace-theme-flash.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin-assets/plugins/bootstrap/css/bootstrap.min.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('https://fonts.googleapis.com/icon?family=Material+Icons')  }}" rel="stylesheet">
    <link href="{{ asset('admin-assets/plugins/jquery-scrollbar/jquery.scrollbar.css')  }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset('admin-assets/plugins/select2/css/select2.min.css')  }}" rel="stylesheet" type="text/css" media="screen" />
    <link class="main-stylesheet" href="{{ asset('admin-assets/css/themes/light.css')  }}" rel="stylesheet" type="text/css" />
    <!-- Please remove the file below for production: Contains demo classes -->
    <link class="main-stylesheet" href="{{ asset('admin-assets/css/style.css')  }}" rel="stylesheet" type="text/css" />
    <style>
        .logo{
            display: flex;
            justify-content: center;
            margin: auto !important;
        }
    </style>
    <script type="text/javascript">
        window.onload = function()
        {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/css/windows.chrome.fix.css')  }}" />'
        }
    </script>
</head>
<body class="fixed-header menu-pin">
<div class="login-wrapper ">
    <!-- START Login Background Pic Wrapper-->
    <div class="bg-pic">
        <img src="{{ asset('admin-assets/img/login-background.png')  }}" data-src="{{ asset('admin-assets/img/login-background.png')  }}" data-src-retina="{{ asset('admin-assets/images/theme/login-bg.jpg')  }}" class="lazy">
        <!-- START Background Caption-->
        <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
            {{--<h5 class="semi-bold text-white">--}}
                {{--{{ ucfirst (config('app.name')) }}, A Direct Application</h2>--}}
            <p class="small">
                {{--That makes simplify your e-Educations. <br>All work copyright of respective--}}
                {{--owner, otherwise <br>--}}
                © {{ date('Y') }} {{ ucfirst (config('app.name')) }}.
            </p>
        </div>
        <!-- END Background Caption-->
    </div>
    <!-- END Login Background Pic Wrapper-->
    <!-- START Login Right Container-->
    <div class="login-container bg-white">
        <div class="p-l-50 p-r-50 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
            <img src="{{ asset('admin-assets/img/logo.png')  }}" alt="logo" data-src="{{ asset('admin-assets/img/logo.png')  }}" data-src-retina="{{ asset('admin-assets/img/logo-48x48_c@2x.png')  }}" width="100" class="logo">
            <h5 class="p-t-25">Login<br/></h5>
            <p class="mw-80 m-t-5">Sign in to your account</p>
            <!-- START Login Form -->
            @if(session('errors'))
                <div class="alert alert-danger">{{ session('errors') }}</div>
            @elseif(session('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>
            @endif
            <form id="form-login" class="p-t-15" role="form" method="post" action="{{  route('admin-auth') }}">
                @csrf
                <div class="form-group form-group-default">
                    <label>Login</label>
                    <div class="controls">
                        <input type="text" name="username" placeholder="User Name" class="form-control" required>
                    </div>
                </div>
                <div class="form-group form-group-default">
                    <label>Password</label>
                    <div class="controls">
                        <input type="password" class="form-control" name="password" placeholder="Credentials" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 d-flex align-items-center justify-content-end">
                        <button aria-label="" class="btn btn-primary btn-lg m-t-10" type="submit">Sign in</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- BEGIN VENDOR JS -->
<script src="{{ asset('admin-assets/plugins/pace/pace.min.js')  }}" type="text/javascript"></script>
<!--  A polyfill for browsers that don't support ligatures: remove liga.js if not needed-->
<script src="{{ asset('admin-assets/plugins/liga.js')  }}" type="text/javascript"></script>
<script src="{{ asset('admin-assets/plugins/jquery/jquery-3.2.1.min.js')  }}" type="text/javascript"></script>
<script src="{{ asset('admin-assets/plugins/modernizr.custom.js')  }}" type="text/javascript"></script>
<script src="{{ asset('admin-assets/plugins/jquery-ui/jquery-ui.min.js')  }}" type="text/javascript"></script>
<script src="{{ asset('admin-assets/plugins/popper/umd/popper.min.js')  }}" type="text/javascript"></script>
<script src="{{ asset('admin-assets/plugins/bootstrap/js/bootstrap.min.js')  }}" type="text/javascript"></script>
<script src="{{ asset('admin-assets/plugins/jquery/jquery-easy.js')  }}" type="text/javascript"></script>
<script src="{{ asset('admin-assets/plugins/jquery-unveil/jquery.unveil.min.js')  }}" type="text/javascript"></script>
<script src="{{ asset('admin-assets/plugins/jquery-ios-list/jquery.ioslist.min.js')  }}" type="text/javascript"></script>
<script src="{{ asset('admin-assets/plugins/jquery-actual/jquery.actual.min.js')  }}" type="text/javascript"></script>
<script src="{{ asset('admin-assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')  }}" type="text/javascript"></script>
<script  src="{{ asset('admin-assets/plugins/select2/js/select2.full.min.js')  }}" type="text/javascript"></script>
<script  src="{{ asset('admin-assets/plugins/classie/classie.js')  }}" type="text/javascript"></script>
<script src="{{ asset('admin-assets/plugins/jquery-validation/js/jquery.validate.min.js')  }}" type="text/javascript"></script>
<!-- END VENDOR JS -->
<script src="{{ asset('admin-assets/js/pages.min.js')  }}"></script>
<script>
    $(function()
    {
        $('#form-login').validate()
    })
</script>
</body>
</html>
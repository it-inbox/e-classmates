@extends('admin.template.layout')
@section('title', 'Edit FAQ')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Faq:faq-list-view,Update:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('faq-list-update') }}" method="post" role="form">
                            @csrf
                            <input type="hidden" name="id" value="{{ $faq->id  }}">
                            <div class="form-group form-group-default">
                                <label for="question">Question<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="question" name="question" value="{{ old('name',$faq->question) }}">
                                @error('question')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group form-group-default">
                                <label for="answer">Answer<span class="text-danger">*</span></label>
                                <textarea class="form-control" id="answer" name="answer" rows="10">{{ old('answer',$faq->answer )}}</textarea>
                                @error('answer')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <div class="form-check form-check-inline">
                                    <input type="radio" name="status" id="radioInline1" value="1" {{ $faq->status == 1 ? 'checked' : ''  }}>
                                    <label for="radioInline1">
                                        Active
                                    </label>
                                </div> <div class="form-check form-check-inline complete">
                                    <input type="radio" name="status" id="radioDisabled2" value="2" {{ $faq->status == 2 ? 'checked' : ''  }}>
                                    <label for="radioDisabled2">
                                        In-active
                                    </label>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="row">
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary btn-md">Update</button>
                                <a href="{{  route('faq-list-view') }}" class="btn text-dark cancel-button">Cancel</a>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
@stop
